﻿using System;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

namespace PlantsApp.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["OracleDb"].ConnectionString;
            OracleConnection connection = new OracleConnection(connectionString);

            var query = "select * from Family";
            connection.Open();

            OracleCommand command = connection.CreateCommand();
            command.CommandText = query;

            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                System.Console.WriteLine( String.Format("Family name - '{0}'", reader["Name"]));
            }
        }
    }
}
