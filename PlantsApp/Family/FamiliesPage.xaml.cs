﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PlantsApp.Entities;
using Autofac;
using PlantsApp.ViewModels;
using PlantsApp.Presenters;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для FamiliesPage.xaml
    /// </summary>
    public partial class FamiliesPage : Page
    {

        private IPresenter<FamilyViewModel> familyPresenter;

        private readonly List<string> searchColumns = new List<string>()
        {
            GridColumns.Name, GridColumns.LatinName, GridColumns.Info
        };

        public FamiliesPage()
        {
            this.familyPresenter = DIContainer.Resolver.Resolve<IPresenter<FamilyViewModel>>();

            InitializeComponent();

            this.familiesList.SelectionChanged += FamiliesList_SelectionChanged;
            this.Loaded += FamiliesPage_Loaded;

            modify_btn.Visibility = Visibility.Hidden;
            del_btn.Visibility = Visibility.Hidden;
        }

        private void FamiliesPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.loadFamilies();
            this.initSearch();
        }

        private void initSearch()
        {
            this.search_field.ItemsSource = searchColumns;
            this.search_field.SelectedItem = searchColumns[0];
            this.search_criteria.Text = string.Empty;
        }

        private void loadFamilies()
        {
            this.familiesList.ItemsSource = familyPresenter.GetAll();
        }

        private void FamiliesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                modify_btn.Visibility = Visibility.Visible;
                del_btn.Visibility = Visibility.Visible;
            }
            else
            {
                modify_btn.Visibility = Visibility.Hidden;
                del_btn.Visibility = Visibility.Hidden;
            }
        }

        private void add_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditFamilyPage(new FamilyViewModel() { FamilyId = -1 }));
        }

        private void navigateTo(Page page)
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.Content = page;
        }

        private void modify_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditFamilyPage((FamilyViewModel)this.familiesList.SelectedItem));
        }

        private void del_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.familiesList.SelectedItem != null)
            {
                var selected = (FamilyViewModel)this.familiesList.SelectedItem;
                familyPresenter.DeleteEntity(selected.FamilyId);
                loadFamilies();
            }
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            var families = familyPresenter.Filter(
                new FilteringModel()
                {
                    Column = search_field.SelectedItem.ToString(),
                    Value = search_criteria.Text
                }).ToList();

            familiesList.ItemsSource = families;
        }

    }
}
