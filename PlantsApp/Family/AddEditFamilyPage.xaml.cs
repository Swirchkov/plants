﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PlantsApp.ViewModels;
using PlantsApp.Presenters;
using Autofac;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для AddEditFamilyPage.xaml
    /// </summary>
    public partial class AddEditFamilyPage : Page
    {
        private FamilyViewModel entity { get; set; }

        private IPresenter<FamilyViewModel> familyPresenter { get; set; }

        public AddEditFamilyPage(FamilyViewModel entity)
        {
            this.familyPresenter = DIContainer.Resolver.Resolve<IPresenter<FamilyViewModel>>();

            InitializeComponent();
            this.entity = entity;

            name_text_box.Text = entity.Name;
            latin_text_box.Text = entity.LatinName;
            info_text_box.Text = entity.Info;

            if (entity.FamilyId != -1)
            {
                headerBlock.Text = "Edit plant family";
            }
            else
            {
                headerBlock.Text = "Create new plant family";
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            entity.Name = name_text_box.Text;
            entity.LatinName = latin_text_box.Text;
            entity.Info = info_text_box.Text;

            if (entity.FamilyId == -1)
            {
                familyPresenter.AddItem(entity);
            }
            else
            {
                familyPresenter.Update(entity);
            }

            goBack();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            goBack();
        }

        private void goBack()
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.GoBack();
        }
    }
}
