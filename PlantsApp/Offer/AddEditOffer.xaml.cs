﻿using PlantsApp.Presenters;
using PlantsApp.ViewModels;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Autofac;
using System.Text.RegularExpressions;
using System;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для AddEditOffer.xaml
    /// </summary>
    public partial class AddEditOffer : Page
    {
        private OfferViewModel entity { get; set; }

        private IPresenter<OfferViewModel> offerPresenter { get; set; }

        private IPresenter<OwnerViewModel> ownerPresenter { get; set; }

        private IPresenter<KindViewModel> kindPresenter { get; set; }


        public AddEditOffer(OfferViewModel offer)
        {
            entity = offer;

            offerPresenter = DIContainer.Resolver.Resolve<IPresenter<OfferViewModel>>();
            kindPresenter = DIContainer.Resolver.Resolve<IPresenter<KindViewModel>>();
            ownerPresenter = DIContainer.Resolver.Resolve<IPresenter<OwnerViewModel>>();

            InitializeComponent();

            if (entity.KindId != -1 && entity.OwnerId != -1)
            {
                headerBlock.Text = "Edit offer";
            }
            else
            {
                headerBlock.Text = "Create new offer";
            }

            this.initControlValues();
        }

        private void initControlValues()
        {
            var kinds = kindPresenter.GetAll();
            kindSelect.ItemsSource = kinds;
            kindSelect.SelectedIndex = kinds.ToList().FindIndex(k => k.KindId == entity.KindId);

            var owners = ownerPresenter.GetAll();
            ownerSelect.ItemsSource = owners;
            ownerSelect.SelectedIndex = owners.ToList().FindIndex(o => o.OwnerId == entity.OwnerId);

            price_text_box.Text = entity.Price.ToString();
            count_text_box.Text = entity.Count.ToString();
            date_select.SelectedDate = entity.PublicationDate != default(DateTime) ? entity.PublicationDate : DateTime.Now;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            bool isAddMode = entity.KindId == -1 && entity.OwnerId == -1;

            entity.KindId = ((KindViewModel)kindSelect.SelectedItem).KindId;
            entity.OwnerId = ((OwnerViewModel)ownerSelect.SelectedItem).OwnerId;

            entity.Price = int.Parse(price_text_box.Text);
            entity.Count = int.Parse(count_text_box.Text);

            entity.PublicationDate = date_select.SelectedDate.HasValue ? date_select.SelectedDate.Value : DateTime.Now;

            if (isAddMode)
            {
                offerPresenter.AddItem(entity);
            }
            else
            {
                offerPresenter.Update(entity);
            }

            goBack();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            goBack();
        }

        private void goBack()
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.GoBack();
        }

        private void digitPreview(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
    }
}
