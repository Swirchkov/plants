﻿using PlantsApp.Entities;
using PlantsApp.Presenters;
using PlantsApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autofac;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для OffersPage.xaml
    /// </summary>
    public partial class OffersPage : Page
    {
        private IPresenter<OfferViewModel> offersPresenter;

        private readonly List<string> searchColumns = new List<string>()
        {
            GridColumns.OwnerName, GridColumns.Price, GridColumns.KindName, GridColumns.Count
        };

        public OffersPage()
        {
            this.offersPresenter = DIContainer.Resolver.Resolve<IPresenter<OfferViewModel>>();

            InitializeComponent();

            this.offersList.SelectionChanged += OffersList_SelectionChanged;
            this.Loaded += OffersPage_Loaded;

            modify_btn.Visibility = Visibility.Hidden;
            del_btn.Visibility = Visibility.Hidden;
        }

        private void OffersPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.loadOffers();
            this.initSearch();
        }

        private void initSearch()
        {
            this.search_field.ItemsSource = searchColumns;
            this.search_field.SelectedItem = searchColumns[0];
            this.search_criteria.Text = string.Empty;
        }

        private void loadOffers()
        {
            this.offersList.ItemsSource = offersPresenter.GetAll();
        }

        private void OffersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                modify_btn.Visibility = Visibility.Visible;
                del_btn.Visibility = Visibility.Visible;
            }
            else
            {
                modify_btn.Visibility = Visibility.Hidden;
                del_btn.Visibility = Visibility.Hidden;
            }
        }

        private void add_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditOffer(new OfferViewModel() { KindId = -1, OwnerId = -1 }));
        }

        private void navigateTo(Page page)
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.Content = page;
        }

        private void modify_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditOffer((OfferViewModel)this.offersList.SelectedItem));
        }

        private void del_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.offersList.SelectedItem != null)
            {
                var selected = (OfferViewModel)this.offersList.SelectedItem;
                offersPresenter.DeleteEntity(selected.OwnerId, selected.KindId);
                loadOffers();
            }
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            var offers = offersPresenter.Filter(
                new FilteringModel()
                {
                    Column = search_field.SelectedItem.ToString(),
                    Value = search_criteria.Text
                }).ToList();

            offersList.ItemsSource = offers;
        }
    }
}
