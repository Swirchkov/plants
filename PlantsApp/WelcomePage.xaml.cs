﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : Page
    {
        public WelcomePage()
        {
            InitializeComponent();
        }

        private void Families_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new FamiliesPage());
        }

        private void Kinds_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new KindsPage());
        }

        private void Owners_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new OwnersPage());
        }

        private void Offers_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new OffersPage());
        }

        private void navigateTo(Page page)
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.Content = page;
        }
    }
}
