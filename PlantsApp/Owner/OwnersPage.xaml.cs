﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PlantsApp.Entities;
using Autofac;
using PlantsApp.ViewModels;
using PlantsApp.Presenters;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для OwnersPage.xaml
    /// </summary>
    public partial class OwnersPage : Page
    {

        private IPresenter<OwnerViewModel> ownerPresenter;

        private readonly List<string> searchColumns = new List<string>()
        {
            GridColumns.Name, GridColumns.Surname, GridColumns.Phone, GridColumns.Address
        };

        public OwnersPage()
        {
            this.ownerPresenter = DIContainer.Resolver.Resolve<IPresenter<OwnerViewModel>>();

            InitializeComponent();

            this.ownersList.SelectionChanged += OwnersList_SelectionChanged;
            this.Loaded += OwnersPage_Loaded;

            //modify_btn.Visibility = Visibility.Hidden;
            //del_btn.Visibility = Visibility.Hidden;
        }

        private void OwnersPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.loadOwners();
            this.initSearch();
        }

        private void initSearch()
        {
            this.search_field.ItemsSource = searchColumns;
            this.search_field.SelectedItem = searchColumns[0];
            this.search_criteria.Text = string.Empty;
        }

        private void loadOwners()
        {
            this.ownersList.ItemsSource = ownerPresenter.GetAll();
        }

        private void OwnersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                modify_btn.Visibility = Visibility.Visible;
                del_btn.Visibility = Visibility.Visible;
            }
            else
            {
                modify_btn.Visibility = Visibility.Hidden;
                del_btn.Visibility = Visibility.Hidden;
            }
        }

        private void add_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditOwnerPage(new OwnerViewModel() { OwnerId = -1 }));
        }

        private void navigateTo(Page page)
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.Content = page;
        }

        private void modify_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditOwnerPage((OwnerViewModel)this.ownersList.SelectedItem));
        }

        private void del_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.ownersList.SelectedItem != null)
            {
                var selected = (OwnerViewModel)this.ownersList.SelectedItem;
                ownerPresenter.DeleteEntity(selected.OwnerId);
                loadOwners();
            }
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            var families = ownerPresenter.Filter(
                new FilteringModel()
                {
                    Column = search_field.SelectedItem.ToString(),
                    Value = search_criteria.Text
                }).ToList();

            ownersList.ItemsSource = families;
        }
    }
}
