﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PlantsApp.ViewModels;
using PlantsApp.Presenters;
using Autofac;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для AddEditOwnerPage.xaml
    /// </summary>
    public partial class AddEditOwnerPage : Page
    {
        private OwnerViewModel entity { get; set; }

        private IPresenter<OwnerViewModel> ownerPresenter { get; set; }

        public AddEditOwnerPage(OwnerViewModel entity)
        {
            this.ownerPresenter = DIContainer.Resolver.Resolve<IPresenter<OwnerViewModel>>();

            InitializeComponent();
            this.entity = entity;

            name_text_box.Text = entity.Name;
            surname_text_box.Text = entity.Surname;
            phone_text_box.Text = entity.Phone;
            address_text_box.Text = entity.Address;

            if (entity.OwnerId != -1)
            {
                headerBlock.Text = "Edit information about owner";
            }
            else
            {
                headerBlock.Text = "Add new owner";
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            entity.Name = name_text_box.Text;
            entity.Surname = surname_text_box.Text;
            entity.Phone = phone_text_box.Text;
            entity.Address = address_text_box.Text;

            if (entity.OwnerId == -1)
            {
                ownerPresenter.AddItem(entity);
            }
            else
            {
                ownerPresenter.Update(entity);
            }

            goBack();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            goBack();
        }

        private void goBack()
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.GoBack();
        }
    }
}
