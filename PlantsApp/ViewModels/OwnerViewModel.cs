﻿using System.ComponentModel;
using PlantsApp.Entities;

namespace PlantsApp.ViewModels
{
    public class OwnerViewModel : INotifyPropertyChanged
    {

        private int ownerId;
        public int OwnerId
        {
            get
            {
                return ownerId;
            }
            set
            {
                ownerId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(OwnerId)));
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
            }
        }

        private string surname;
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Surname)));
            }
        }

        private string phone;
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Phone)));
            }
        }

        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Address)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public OwnerViewModel() { }

        public OwnerViewModel(Owner owner)
        {
            OwnerId = owner.OwnerId;
            Name = owner.Name;
            Surname = owner.Surname;
            Address = owner.Address;
            Phone = owner.Phone;
        }

        public Owner ToEntity()
        {
            return new Owner()
            {
                OwnerId = OwnerId,
                Name = Name,
                Surname = Surname,
                Address = Address,
                Phone = Phone
            };
        }

    }
}
