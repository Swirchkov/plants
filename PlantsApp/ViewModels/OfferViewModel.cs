﻿using System;
using System.ComponentModel;
using PlantsApp.Entities;

namespace PlantsApp.ViewModels
{
    public class OfferViewModel : INotifyPropertyChanged 
    {
        
        private int kindId;
        public int KindId
        {
            get
            {
                return kindId;
            }
            set
            {
                kindId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(KindId)));
            }
        }

        private int ownerId;
        public int OwnerId
        {
            get
            {
                return ownerId;
            }
            set
            {
                ownerId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(OwnerId)));
            }
        }

        private int price;
        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Price)));
            }
        }

        private DateTime publication;
        public DateTime PublicationDate
        {
            get
            {
                return publication;
            }
            set
            {
                publication = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PublicationDate)));
            }
        }

        private int count;
        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                count = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Count)));
            }
        }

        private string kindName;
        public string KindName
        {
            get
            {
                return kindName;
            }
            set
            {
                kindName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(KindName)));
            }
        }

        private string ownerName;
        public string OwnerName
        {
            get
            {
                return ownerName;
            }
            set
            {
                ownerName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(OwnerName)));
            }
        }

        public string OfferView
        {
            get
            {
                return String.Format("{0} ( {1} )", KindName, OwnerName);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public OfferViewModel() { }

        public OfferViewModel(Offer offer)
        {
            OwnerId = offer.OwnerId;
            OwnerName = offer.OwnerName;

            KindId = offer.KindId;
            KindName = offer.KindName;

            Price = offer.Price;
            PublicationDate = offer.PublicationDate;
            Count = offer.Count;
        }

        public Offer ToEntity()
        {
            return new Offer()
            {
                KindId = KindId,
                KindName = KindName,

                OwnerId = OwnerId,
                OwnerName = OwnerName,

                Price = Price,
                PublicationDate = PublicationDate,
                Count = Count
            };
        }

    }
}
