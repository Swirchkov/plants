﻿using System.ComponentModel;
using PlantsApp.Entities;

namespace PlantsApp.ViewModels
{
    public class FamilyViewModel : INotifyPropertyChanged 
    {
        private int familyId;
        public int FamilyId
        {
            get
            {
                return familyId;
            }
            set
            {
                familyId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FamilyId)));
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
            }
        }

        private string latinName;
        public string LatinName
        {
            get
            {
                return latinName;
            }
            set
            {
                latinName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LatinName)));
            }
        }

        private string info;
        public string Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Info)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public FamilyViewModel() { }

        public FamilyViewModel(Family family)
        {
            FamilyId = family.FamilyId;
            Name = family.Name;
            LatinName = family.LatinName;
            Info = family.Info;
        }

        public Family ToEntity()
        {
            return new Family()
            {
                FamilyId = FamilyId,
                Name = Name,
                LatinName = LatinName,
                Info = Info
            };
        }
    }
}
