﻿using System.ComponentModel;
using PlantsApp.Entities;

namespace PlantsApp.ViewModels
{
    public class KindViewModel : INotifyPropertyChanged 
    {
        private int kindId;
        public int KindId
        {
            get
            {
                return kindId;
            }
            set
            {
                kindId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(KindId)));
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
            }
        }

        private string latinName;
        public string LatinName
        {
            get
            {
                return latinName;
            }
            set
            {
                latinName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LatinName)));
            }
        }

        private string info;
        public string Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Info)));
            }
        }

        private int startPrice;
        public int StartPrice
        {
            get
            {
                return startPrice;
            }
            set
            {
                startPrice = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(StartPrice)));
            }
        }

        private int familyId;
        public int FamilyId
        {
            get
            {
                return familyId;
            }
            set
            {
                familyId = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FamilyId)));
            }
        }

        private string familyName;
        public string FamilyName
        {
            get
            {
                return familyName;
            }
            set
            {
                familyName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FamilyName)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public KindViewModel() { }

        public KindViewModel(Kind kind)
        {
            KindId = kind.KindId;
            Name = kind.Name;

            LatinName = kind.LatinName;
            Info = kind.Info;
            StartPrice = kind.StartPrice;

            FamilyId = kind.FamilyId;
            FamilyName = kind.FamilyName;
        }

        public Kind ToEntity()
        {
            return new Kind()
            {
                KindId = KindId,
                Name = Name,

                StartPrice = StartPrice,
                Info = Info,
                LatinName = LatinName,

                FamilyId = FamilyId,
                FamilyName = FamilyName
            };
        }

    }
}
