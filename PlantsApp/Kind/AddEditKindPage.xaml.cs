﻿using PlantsApp.Presenters;
using PlantsApp.ViewModels;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Autofac;
using System.Text.RegularExpressions;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для AddEditKindPage.xaml
    /// </summary>
    public partial class AddEditKindPage : Page
    {
        private KindViewModel entity { get; set; }

        private IPresenter<KindViewModel> kindPresenter { get; set; }

        private IPresenter<FamilyViewModel> familyPresenter { get; set; }

        public AddEditKindPage(KindViewModel kind)
        {
            entity = kind;
            kindPresenter = DIContainer.Resolver.Resolve<IPresenter<KindViewModel>>();
            familyPresenter = DIContainer.Resolver.Resolve<IPresenter<FamilyViewModel>>();

            InitializeComponent();

            if (entity.KindId != -1)
            {
                headerBlock.Text = "Edit plant kind";
            }
            else
            {
                headerBlock.Text = "Create new plant kind";
            }

            this.initControlValues();
        }

        private void initControlValues()
        {
            name_text_box.Text = entity.Name;
            latin_text_box.Text = entity.LatinName;
            info_text_box.Text = entity.Info;
            start_price_text_box.Text = entity.StartPrice.ToString();

            var families = familyPresenter.GetAll();
            familySelect.ItemsSource = families;
            familySelect.SelectedIndex = families.ToList().FindIndex(f => f.FamilyId == entity.FamilyId);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            entity.Name = name_text_box.Text;
            entity.LatinName = latin_text_box.Text;
            entity.Info = info_text_box.Text;
            entity.StartPrice = int.Parse(start_price_text_box.Text);
            entity.FamilyId = ((FamilyViewModel)familySelect.SelectedItem).FamilyId;

            if (entity.KindId == -1)
            {
                kindPresenter.AddItem(entity);
            }
            else
            {
                kindPresenter.Update(entity);
            }

            goBack();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            goBack();
        }

        private void goBack()
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.GoBack();
        }

        private void digitPreview(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
    }
}
