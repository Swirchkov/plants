﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using PlantsApp.Entities;
using Autofac;
using PlantsApp.ViewModels;
using PlantsApp.Presenters;

namespace PlantsApp
{
    /// <summary>
    /// Логика взаимодействия для KindsPage.xaml
    /// </summary>
    public partial class KindsPage : Page
    {
        private IPresenter<KindViewModel> kindsPresenter;

        private readonly List<string> searchColumns = new List<string>()
        {
            GridColumns.Name, GridColumns.LatinName, GridColumns.Info, GridColumns.StartPrice, GridColumns.FamilyName
        };

        public KindsPage()
        {
            this.kindsPresenter = DIContainer.Resolver.Resolve<IPresenter<KindViewModel>>();

            InitializeComponent();

            this.kindsList.SelectionChanged += KindsList_SelectionChanged;
            this.Loaded += KindsPage_Loaded;

            modify_btn.Visibility = Visibility.Hidden;
            del_btn.Visibility = Visibility.Hidden;
        }

        private void KindsPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.loadKinds();
            this.initSearch();
        }

        private void initSearch()
        {
            this.search_field.ItemsSource = searchColumns;
            this.search_field.SelectedItem = searchColumns[0];
            this.search_criteria.Text = string.Empty;
        }

        private void loadKinds()
        {
            this.kindsList.ItemsSource = kindsPresenter.GetAll();
        }

        private void KindsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                modify_btn.Visibility = Visibility.Visible;
                del_btn.Visibility = Visibility.Visible;
            }
            else
            {
                modify_btn.Visibility = Visibility.Hidden;
                del_btn.Visibility = Visibility.Hidden;
            }
        }

        private void add_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditKindPage(new KindViewModel() { KindId = -1 }));
        }

        private void navigateTo(Page page)
        {
            NavigationWindow window = (NavigationWindow)Window.GetWindow(this);
            window.Content = page;
        }

        private void modify_btn_Click(object sender, RoutedEventArgs e)
        {
            this.navigateTo(new AddEditKindPage((KindViewModel)this.kindsList.SelectedItem));
        }

        private void del_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.kindsList.SelectedItem != null)
            {
                var selected = (KindViewModel)this.kindsList.SelectedItem;
                kindsPresenter.DeleteEntity(selected.KindId);
                loadKinds();
            }
        }

        private void Filter_Click(object sender, RoutedEventArgs e)
        {
            var kinds = kindsPresenter.Filter(
                new FilteringModel()
                {
                    Column = search_field.SelectedItem.ToString(),
                    Value = search_criteria.Text
                }).ToList();

            kindsList.ItemsSource = kinds;
        }
    }
}
