﻿using System.Collections.Generic;
using System.Linq;
using PlantsApp.ViewModels;
using PlantsApp.DAL;
using PlantsApp.Entities;
using Autofac;
using System;

namespace PlantsApp.Presenters
{
    class OfferPresenter : IPresenter<OfferViewModel>
    {
        private IRepository<Offer> offerRepo;

        public OfferPresenter()
        {
            offerRepo = DIContainer.Resolver.Resolve<IRepository<Offer>>();
        }

        public void AddItem(OfferViewModel item)
        {
            offerRepo.Add(item.ToEntity());
        }

        public void DeleteEntity(params int[] ids)
        {
            offerRepo.Delete(ids);
        }

        public IEnumerable<OfferViewModel> Filter(FilteringModel filterBy)
        {
            return offerRepo.FilterBy(filterBy).Select(f => new OfferViewModel(f));
        }

        public IEnumerable<OfferViewModel> GetAll()
        {
            return offerRepo.GetAll().Select(o => new OfferViewModel(o));
        }

        public void Update(OfferViewModel item)
        {
            offerRepo.Update(item.ToEntity());
        }
    }
}
