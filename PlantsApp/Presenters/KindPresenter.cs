﻿using System.Collections.Generic;
using System.Linq;
using PlantsApp.ViewModels;
using PlantsApp.DAL;
using PlantsApp.Entities;
using Autofac;

namespace PlantsApp.Presenters
{
    class KindPresenter : IPresenter<KindViewModel>
    {
        private IRepository<Kind> kindRepo;

        public KindPresenter()
        {
            kindRepo = DIContainer.Resolver.Resolve<IRepository<Kind>>();
        }

        public void DeleteEntity(params int[] ids)
        {
            kindRepo.Delete(ids);
        }

        public IEnumerable<KindViewModel> Filter(FilteringModel filterBy)
        {
            return kindRepo.FilterBy(filterBy).Select(k => new KindViewModel(k));
        }

        public IEnumerable<KindViewModel> GetAll()
        {
            return kindRepo.GetAll().Select(k => new KindViewModel(k));
        }

        public void AddItem(KindViewModel kind)
        {
            int kindId = kindRepo.Add(kind.ToEntity());
            kind.KindId = kindId;
        }

        public void Update(KindViewModel kind)
        {
            kindRepo.Update(kind.ToEntity());
        }
    }
}
