﻿using PlantsApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantsApp.Presenters
{
    internal interface IPresenter<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Filter(FilteringModel filterBy);
        void AddItem(T item);
        void Update(T item);
        void DeleteEntity(params int[] ids);
    }
}
