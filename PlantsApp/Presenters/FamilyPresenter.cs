﻿using System.Collections.Generic;
using System.Linq;
using PlantsApp.ViewModels;
using PlantsApp.DAL;
using PlantsApp.Entities;
using Autofac;

namespace PlantsApp.Presenters
{
    class FamilyPresenter : IPresenter<FamilyViewModel>
    {
        private IRepository<Family> familyRepo;

        public FamilyPresenter()
        {
            familyRepo = DIContainer.Resolver.Resolve<IRepository<Family>>();
        }

        public void DeleteEntity(params int[] ids)
        {
            familyRepo.Delete(ids);
        }

        public IEnumerable<FamilyViewModel> Filter(FilteringModel filterBy)
        {
            return familyRepo.FilterBy(filterBy).Select(f => new FamilyViewModel(f));
        }

        public IEnumerable<FamilyViewModel> GetAll()
        {
            return familyRepo.GetAll().Select(f => new FamilyViewModel(f));
        }

        public void AddItem(FamilyViewModel item)
        {
            int itemId = familyRepo.Add(item.ToEntity());
            item.FamilyId = itemId;
        }

        public void Update(FamilyViewModel item)
        {
            familyRepo.Update(item.ToEntity());
        }

    }
}
