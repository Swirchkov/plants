﻿using System.Collections.Generic;
using System.Linq;
using PlantsApp.ViewModels;
using PlantsApp.DAL;
using PlantsApp.Entities;
using Autofac;

namespace PlantsApp.Presenters
{
    class OwnerPresenter : IPresenter<OwnerViewModel>
    {
        private IRepository<Owner> ownerRepo;

        public OwnerPresenter()
        {
            ownerRepo = DIContainer.Resolver.Resolve<IRepository<Owner>>();
        }

        public void DeleteEntity(params int[] ids)
        {
            ownerRepo.Delete(ids);
        }

        public IEnumerable<OwnerViewModel> Filter(FilteringModel filterBy)
        {
            return ownerRepo.FilterBy(filterBy).Select(o => new OwnerViewModel(o));
        }

        public IEnumerable<OwnerViewModel> GetAll()
        {
            return ownerRepo.GetAll().Select(o => new OwnerViewModel(o));
        }

        public void AddItem(OwnerViewModel item)
        {
            int itemId = ownerRepo.Add(item.ToEntity());
            item.OwnerId = itemId;
        }

        public void Update(OwnerViewModel item)
        {
            ownerRepo.Update(item.ToEntity());
        }
    }
}
