﻿using Autofac;
using PlantsApp.DAL;
using PlantsApp.Presenters;
using PlantsApp.ViewModels;

namespace PlantsApp
{
    public static class DIContainer
    {
        static DIContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule<DalModule>();

            builder.RegisterType<FamilyPresenter>().As<IPresenter<FamilyViewModel>>();
            builder.RegisterType<KindPresenter>().As<IPresenter<KindViewModel>>();
            builder.RegisterType<OfferPresenter>().As<IPresenter<OfferViewModel>>();
            builder.RegisterType<OwnerPresenter>().As<IPresenter<OwnerViewModel>>();

            Resolver = builder.Build();
        }

        public static IContainer Resolver { get; set; }
    }
}
