﻿#pragma checksum "..\..\..\Kind\AddEditKindPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "006E479028107071F17241D5D95B20AE"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using PlantsApp;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PlantsApp {
    
    
    /// <summary>
    /// AddEditKindPage
    /// </summary>
    public partial class AddEditKindPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock headerBlock;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox name_text_box;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox latin_text_box;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox info_text_box;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox start_price_text_box;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\Kind\AddEditKindPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox familySelect;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PlantsApp;component/kind/addeditkindpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Kind\AddEditKindPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.headerBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.name_text_box = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.latin_text_box = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.info_text_box = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.start_price_text_box = ((System.Windows.Controls.TextBox)(target));
            
            #line 79 "..\..\..\Kind\AddEditKindPage.xaml"
            this.start_price_text_box.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.digitPreview);
            
            #line default
            #line hidden
            return;
            case 6:
            this.familySelect = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            
            #line 110 "..\..\..\Kind\AddEditKindPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Save_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 114 "..\..\..\Kind\AddEditKindPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Cancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

