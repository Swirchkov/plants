﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlantsApp.Entities;

namespace PlantsApp.DAL
{
    static class Data
    {
        #region Families

        internal static ICollection<Family> Families => families;

        private static ICollection<Family> families = new List<Family>()
        {
            new Family()
            {
                FamilyId = 0,
                Name = "Family 1",
                LatinName = "Latin family 1",
                Info = "info 1111 info"
            },
            new Family()
            {
                FamilyId = 1,
                Name = "Family 2",
                LatinName = "Latin family 2",
                Info = "info 2222 info"
            },
            new Family()
            {
                FamilyId = 2,
                Name = "Family 3",
                LatinName = "Latin family 3",
                Info = "info 3333 info"
            },
            new Family()
            {
                FamilyId = 3,
                Name = "Family 4",
                LatinName = "Latin family 4",
                Info = "info 4444 info"
            },
            new Family()
            {
                FamilyId = 4,
                Name = "Family 5",
                LatinName = "Latin family 5",
                Info = "info 5555 info"
            },
            new Family()
            {
                FamilyId = 5,
                Name = "Family 6",
                LatinName = "Latin family 6",
                Info = "info 6666 info"
            }
        };
        #endregion

        #region Kinds

        internal static ICollection<Kind> Kinds => kinds;

        private static ICollection<Kind> kinds = new List<Kind>()
        {
            new Kind()
            {
                KindId = 0,
                FamilyId = 0,
                Info = "kind info 1111 info",
                LatinName = "latin kind 1",
                Name = "kind name 1",
                StartPrice = 11
            },
            new Kind()
            {
                KindId = 1,
                FamilyId = 1,
                Info = "kind info 2222 info",
                LatinName = "latin kind 2",
                Name = "kind name 2",
                StartPrice = 21
            },
            new Kind()
            {
                KindId = 2,
                FamilyId = 2,
                Info = "kind info 3333 info",
                LatinName = "latin kind 3",
                Name = "kind name 3",
                StartPrice = 31
            },
            new Kind()
            {
                KindId = 3,
                FamilyId = 3,
                Info = "kind info 4444 info",
                LatinName = "latin kind 4",
                Name = "kind name 4",
                StartPrice = 41
            },
            new Kind()
            {
                KindId = 4,
                FamilyId = 4,
                Info = "kind info 5555 info",
                LatinName = "latin kind 5",
                Name = "kind name 5",
                StartPrice = 51
            },
            new Kind()
            {
                KindId = 5,
                FamilyId = 5,
                Info = "kind info 6666 info",
                LatinName = "latin kind 6",
                Name = "name 6",
                StartPrice = 61
            }
        };

        #endregion

        #region Owners

        internal static ICollection<Owner> Owners => owners;

        private static ICollection<Owner> owners = new List<Owner>()
        {
            new Owner()
            {
                OwnerId = 0,
                Name = "Owner name 1",
                Surname = "Surname 1",
                Address = "Address 1",
                Phone = "(011) 111 11 11"
            },
            new Owner()
            {
                OwnerId = 1,
                Name = "Owner name 2",
                Surname = "Surname 2",
                Address = "Address 2",
                Phone = "(022) 222 22 22"
            },
            new Owner()
            {
                OwnerId = 2,
                Name = "Owner name 3",
                Surname = "Surname 3",
                Address = "Address 3",
                Phone = "(033) 333 33 33",
            },
            new Owner()
            {
                OwnerId = 3,
                Name = "Owner name 4",
                Surname = "Surname 4",
                Address = "Address 4",
                Phone = "(044) 444 44 44",
            },
            new Owner()
            {
                OwnerId = 4,
                Name = "Owner name 5",
                Surname = "Surname 5",
                Address = "Address 5",
                Phone = "(055) 555 55 55",
            },
            new Owner()
            {
                OwnerId = 5,
                Name = "Owner name 6",
                Surname = "Surname 6",
                Address = "Address 6",
                Phone = "(066) 666 66 66",
            }
        };

        #endregion

        #region Offers

        internal static ICollection<Offer> Offers => offers;

        private static ICollection<Offer> offers = new List<Offer>()
        {
            new Offer()
            {
                OwnerId = 0,
                KindId = 0,
                Count = 1,
                Price = 11,
                PublicationDate = new DateTime(2017, 1, 1)
            },
            new Offer()
            {
                OwnerId = 1,
                KindId = 1,
                Count = 2,
                Price = 22,
                PublicationDate = new DateTime(2017, 2, 2)
            },
            new Offer()
            {
                OwnerId = 2,
                KindId = 2,
                Count = 3,
                Price = 33,
                PublicationDate = new DateTime(2017, 3, 3)
            },
            new Offer()
            {
                OwnerId = 3,
                KindId = 3,
                Count = 4,
                Price = 44,
                PublicationDate = new DateTime(2017, 4, 4)
            },
            new Offer()
            {
                OwnerId = 4,
                KindId = 4,
                Count = 5,
                Price = 55,
                PublicationDate = new DateTime(2017, 5, 5)
            },
            new Offer()
            {
                OwnerId = 5,
                KindId = 5,
                Count = 6,
                Price = 66,
                PublicationDate = new DateTime(2017, 6, 6)
            }
        };

        #endregion
    }
}
