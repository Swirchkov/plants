﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantsApp.DAL
{
    public class EnumerablePaged<T> where T: class
    {
        public IEnumerable<T> Items { get; set; }
        public int Count { get; set; }
        public int TotalSize { get; set; }
    }
}
