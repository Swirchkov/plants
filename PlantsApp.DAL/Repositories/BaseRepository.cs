﻿
using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;

namespace PlantsApp.DAL
{
    abstract class BaseRepository
    {
        protected void getConnection(Action<OracleConnection> dbWorker)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OracleDb"].ConnectionString;

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                dbWorker.Invoke(connection);
                connection.Close();
            }
        }

        protected T getConnection<T>(Func<OracleConnection, T> dbWorker)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OracleDb"].ConnectionString;
            T item = default(T);

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                item = dbWorker.Invoke(connection);
                connection.Close();
            }

            return item;
        }
    }
}
