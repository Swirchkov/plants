﻿using System;
using System.Collections.Generic;
using PlantsApp.Entities;
using Oracle.ManagedDataAccess.Client;

namespace PlantsApp.DAL.Repositories
{
    internal class OfferRepository : BaseRepository, IRepository<Offer>
    {
        private readonly string SELECT_ALL_OFFERS =  @"
            SELECT Offer.Kind_Id, Offer.Owner_Id, Offer.Count, Offer.Date_Of_Publication, Offer.Price, 
                    Kind.Name as KindName, Owner.Name as OwnerName
            FROM Offer 
            JOIN Kind ON Kind.Kind_Id = Offer.Kind_Id 
            JOIN Owner ON Owner.Owner_Id = Offer.Owner_Id";

        private readonly string SELECT_FILTERED_OFFERS = @"
            SELECT Offer.Kind_Id, Offer.Owner_Id, Offer.Count, Offer.Date_Of_Publication, Offer.Price, 
                    Kind.Name as KindName, Owner.Name as OwnerName
            FROM Offer 
            JOIN Kind ON Kind.Kind_Id = Offer.Kind_Id 
            JOIN Owner ON Owner.Owner_Id = Offer.Owner_Id
            WHERE {0}=:{1}";

        private readonly string UPDATE_OFFER = "UPDATE Offer SET Price=:Price, Date_Of_Publication=:DateOfPublication, Count=:Count WHERE Kind_Id=:KindId AND Owner_Id = :OwnerId";
        private readonly string DELETE_OFFER = "DELETE FROM Offer WHERE Owner_Id=:OwnerId AND Kind_Id = :KindId";

        public int Add(Offer item)
        {
            Data.Offers.Add(item);
            return default(int);
        }

        public void Delete(params int[] id)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = DELETE_OFFER;
                command.Parameters.Add(new OracleParameter("OwnerId", id[0]));
                command.Parameters.Add(new OracleParameter("KindId", id[1]));

                command.ExecuteNonQuery();
            });
        }

        public IEnumerable<Offer> FilterBy(FilteringModel filter)
        {
            if (!String.IsNullOrWhiteSpace(filter.Value))
            {
                var command = new OracleCommand(string.Format(SELECT_FILTERED_OFFERS, filter.Column, filter.Column));
                command.Parameters.Add(new OracleParameter(filter.Column, filter.Value));
                return getOffersByCommand(command);
            }
            else
            {
                return GetAll();
            }
        }

        public IEnumerable<Offer> GetAll()
        {
            return getOffersByCommand(new OracleCommand(SELECT_ALL_OFFERS));
        }

        public void Update(Offer item)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = UPDATE_OFFER;
                command.CommandType = System.Data.CommandType.Text;

                command.Parameters.Add(new OracleParameter("Price", item.Price));
                command.Parameters.Add(new OracleParameter("DateOfPublication", item.PublicationDate));
                command.Parameters.Add(new OracleParameter("Count", item.Count));
                command.Parameters.Add(new OracleParameter("KindId", item.KindId));
                command.Parameters.Add(new OracleParameter("OwnerId", item.OwnerId));

                command.Prepare();
                command.ExecuteNonQuery();
            });
        }

        private IEnumerable<Offer> getOffersByCommand(OracleCommand command)
        {
            return getConnection<IEnumerable<Offer>>(connection =>
            {
                command.Connection = connection;

                var reader = command.ExecuteReader();
                List<Offer> kinds = new List<Offer>();

                while (reader.Read())
                {
                    kinds.Add(new Offer()
                    {
                        KindId = (int)(decimal)reader["Kind_ID"],
                        OwnerId = (int)(decimal)reader["Owner_Id"],
                        Price = (int)(decimal)reader["Price"],
                        PublicationDate = (DateTime)reader["Date_Of_Publication"],
                        Count = (int)(decimal)reader["Count"],
                        KindName = (string)reader["KindName"],
                        OwnerName = (string)reader["OwnerName"]
                    });
                }

                return kinds;
            });
        }
    }
}
