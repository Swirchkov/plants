﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlantsApp.Entities;
using Oracle.ManagedDataAccess.Client;

namespace PlantsApp.DAL
{
    internal class FamilyRepository : BaseRepository, IRepository<Family>
    {
        private int nextItemId = Data.Families.Count;

        private readonly string SELECT_ALL_FAMILIES = "select * from Family";
        private readonly string SELECT_FILTERED_FAMILIES = "SELECT * FROM Family WHERE {0}=:{1}";
        private readonly string UPDATE_FAMILY = "UPDATE FAMILY SET Name=:Name, LatinName=:LatinName, Info=:Info WHERE Family_Id=:FamilyId";
        private readonly string DELETE_FAMILY = "DELETE FROM Family WHERE Family_Id=:FamilyId";

        public int Add(Family item)
        {
            item.FamilyId = nextItemId++;
            Data.Families.Add(item);
            return item.FamilyId;
        }

        public void Delete(params int[] id)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = DELETE_FAMILY;
                command.Parameters.Add(new OracleParameter("FamilyId", id[0]));

                command.ExecuteNonQuery();
            });
        }

        public IEnumerable<Family> FilterBy(FilteringModel filter)
        {
            if (!String.IsNullOrWhiteSpace(filter.Value))
            {
                var command = new OracleCommand(string.Format(SELECT_FILTERED_FAMILIES, filter.Column, filter.Column));
                command.Parameters.Add(new OracleParameter(filter.Column, filter.Value));
                return getFamiliesByCommand(command);
            }
            else
            {
                return GetAll();
            }
        }

        public IEnumerable<Family> GetAll()
        {
            return getFamiliesByCommand(new OracleCommand(SELECT_ALL_FAMILIES));
        }

        public void Update(Family item)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = UPDATE_FAMILY;
                command.CommandType = System.Data.CommandType.Text;
                command.Parameters.Add(new OracleParameter("Name", item.Name));
                command.Parameters.Add(new OracleParameter("LatinName", item.LatinName));
                command.Parameters.Add(new OracleParameter("Info", item.Info));
                command.Parameters.Add(new OracleParameter("FamilyId", item.FamilyId));

                command.Prepare();
                command.ExecuteNonQuery();
            });
        }

        private IEnumerable<Family> getFamiliesByCommand(OracleCommand command)
        {
            return getConnection<IEnumerable<Family>>(connection =>
            {
                command.Connection = connection;

                var reader = command.ExecuteReader();
                List<Family> families = new List<Family>();

                while (reader.Read())
                {
                    families.Add(new Family()
                    {
                        FamilyId = (int)(decimal)reader["Family_Id"],
                        Name = (string)reader["Name"],
                        LatinName = (string)reader["LatinName"],
                        Info = (string)reader["Info"]
                    });
                }

                return families;
            });
        }
    }
}
