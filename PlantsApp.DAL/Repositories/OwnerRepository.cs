﻿using System;
using System.Collections.Generic;
using PlantsApp.Entities;
using Oracle.ManagedDataAccess.Client;

namespace PlantsApp.DAL.Repositories
{
    internal class OwnerRepository : BaseRepository, IRepository<Owner>
    {
        private int nextItemId = Data.Owners.Count;

        private readonly string SELECT_ALL_OWNERS = "select * from Owner";
        private readonly string SELECT_FILTERED_OWNERS = "SELECT * FROM Owner WHERE {0}=:{1}";
        private readonly string UPDATE_OWNER = @"UPDATE Owner SET Name=:Name, Surname=:Surname, Phone=:Phone, 
            Address = :Address WHERE Owner_Id=:OwnerId";
        private readonly string DELETE_OWNER = "DELETE FROM Owner WHERE Owner_Id=:OwnerId";

        public int Add(Owner item)
        {
            item.OwnerId = nextItemId++;
            Data.Owners.Add(item);
            return item.OwnerId;
        }

        public void Delete(params int[] id)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = DELETE_OWNER;
                command.Parameters.Add(new OracleParameter("OwnerId", id[0]));

                command.ExecuteNonQuery();
            });
        }

        public IEnumerable<Owner> FilterBy(FilteringModel filter)
        {
            if (!String.IsNullOrWhiteSpace(filter.Value))
            {
                var command = new OracleCommand(string.Format(SELECT_FILTERED_OWNERS, filter.Column, filter.Column));
                command.Parameters.Add(new OracleParameter(filter.Column, filter.Value));
                return getOwnersByCommand(command);
            }
            else
            {
                return GetAll();
            }
        }

        public IEnumerable<Owner> GetAll()
        {
            return getOwnersByCommand(new OracleCommand(SELECT_ALL_OWNERS));
        }

        public void Update(Owner item)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = UPDATE_OWNER;
                command.CommandType = System.Data.CommandType.Text;
                command.Parameters.Add(new OracleParameter("Name", item.Name));
                command.Parameters.Add(new OracleParameter("Surname", item.Surname));
                command.Parameters.Add(new OracleParameter("Phone", item.Phone));
                command.Parameters.Add(new OracleParameter("Address", item.Address));
                command.Parameters.Add(new OracleParameter("OwnerId", item.OwnerId));

                command.Prepare();
                command.ExecuteNonQuery();
            });
        }

        private IEnumerable<Owner> getOwnersByCommand(OracleCommand command)
        {
            return getConnection<IEnumerable<Owner>>(connection =>
            {
                command.Connection = connection;

                var reader = command.ExecuteReader();
                List<Owner> owners = new List<Owner>();

                while (reader.Read())
                {
                    owners.Add(new Owner()
                    {
                        OwnerId = (int)(decimal)reader["Owner_ID"],
                        Name = (string)reader["Name"],
                        Surname = (string)reader["Surname"],
                        Phone = (string)reader["Phone"],
                        Address = (string)reader["Address"]
                    });
                }

                return owners;
            });
        }
    }
}
