﻿using System;
using System.Collections.Generic;
using PlantsApp.Entities;
using Oracle.ManagedDataAccess.Client;

namespace PlantsApp.DAL.Repositories
{
    internal class KindRepository : BaseRepository, IRepository<Kind>
    {
        private int nextItemId = Data.Kinds.Count;

        private readonly string SELECT_ALL_KINDS = @"
            select Kind.Kind_ID, Kind.Name as KindName, Kind.Latin_Name, Kind.Info, Kind.Start_Price, Kind.Family_Id, 
                    Family.Name as FamilyName 
            FROM Kind
            JOIN Family ON Kind.Family_Id = Family.Family_Id";

        private readonly string SELECT_FILTERED_KINDS = @"
            select Kind.Kind_ID, Kind.Name as KindName, Kind.Latin_Name, Kind.Info, Kind.Start_Price, Kind.Family_Id, 
                    Family.Name as FamilyName 
            FROM Kind
            JOIN Family ON Kind.Family_Id = Family.Family_Id
            WHERE {0}=:{1}";

        private readonly string UPDATE_KIND = "UPDATE Kind SET Name=:Name, Latin_Name=:LatinName, Info=:Info, Start_Price=:StartPrice, Family_ID=:FamilyId WHERE Kind_Id=:KindId";
        private readonly string DELETE_KIND = "DELETE FROM Kind WHERE Kind_Id=:KindId";

        public int Add(Kind item)
        {
            item.KindId = nextItemId++;
            Data.Kinds.Add(item);
            return item.KindId;

        }

        public void Delete(params int[] id)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = DELETE_KIND;
                command.Parameters.Add(new OracleParameter("KindId", id[0]));

                command.ExecuteNonQuery();
            });
        }

        public IEnumerable<Kind> FilterBy(FilteringModel filter)
        {
            if (!String.IsNullOrWhiteSpace(filter.Value))
            {
                var command = new OracleCommand(string.Format(SELECT_FILTERED_KINDS, filter.Column, filter.Column));
                command.Parameters.Add(new OracleParameter(filter.Column, filter.Value));
                return getKindsByCommand(command);
            }
            else
            {
                return GetAll();
            }
        }

        public IEnumerable<Kind> GetAll()
        {
            return getKindsByCommand(new OracleCommand(SELECT_ALL_KINDS));
        }

        public void Update(Kind item)
        {
            getConnection(connection =>
            {
                var command = connection.CreateCommand();

                command.CommandText = UPDATE_KIND;
                command.CommandType = System.Data.CommandType.Text;
                command.Parameters.Add(new OracleParameter("Name", item.Name));
                command.Parameters.Add(new OracleParameter("LatinName", item.LatinName));
                command.Parameters.Add(new OracleParameter("Info", item.Info));
                command.Parameters.Add(new OracleParameter("StartPrice", item.StartPrice));
                command.Parameters.Add(new OracleParameter("FamilyId", item.FamilyId));
                command.Parameters.Add(new OracleParameter("KindId", item.KindId));

                command.Prepare();
                command.ExecuteNonQuery();
            });
        }

        private IEnumerable<Kind> getKindsByCommand(OracleCommand command)
        {
            return getConnection<IEnumerable<Kind>>(connection =>
            {
                command.Connection = connection;

                var reader = command.ExecuteReader();
                List<Kind> kinds = new List<Kind>();

                while (reader.Read())
                {
                    kinds.Add(new Kind()
                    {
                        FamilyId = (int)(decimal)reader["Family_ID"],
                        Name = (string)reader["KindName"],
                        LatinName = (string)reader["Latin_Name"],
                        Info = (string)reader["Info"],
                        StartPrice = (int)(decimal)reader["Start_Price"],
                        KindId = (int)(decimal)reader["Kind_ID"],
                        FamilyName = (string)reader["FamilyName"]
                    });
                }

                return kinds;
            });
        }
    }
}
