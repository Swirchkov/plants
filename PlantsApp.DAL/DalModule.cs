﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using PlantsApp.Entities;
using PlantsApp.DAL.Repositories;

namespace PlantsApp.DAL
{
    public class DalModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<OfferRepository>().As<IRepository<Offer>>();
            builder.RegisterType<OwnerRepository>().As<IRepository<Owner>>();
            builder.RegisterType<FamilyRepository>().As<IRepository<Family>>();
            builder.RegisterType<KindRepository>().As<IRepository<Kind>>();

            base.Load(builder);
        }
    }
}
