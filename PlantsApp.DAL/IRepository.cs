﻿using PlantsApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantsApp.DAL
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> FilterBy(FilteringModel filter);
        int Add(T item);
        void Update(T item);
        void Delete(params int[] id);
    }
}
