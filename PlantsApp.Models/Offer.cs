﻿using System;

namespace PlantsApp.Entities
{
    public class Offer
    {
        public int KindId { get; set; }
        public int OwnerId { get; set; }
        public int Price { get; set; }
        public DateTime PublicationDate { get; set; }
        public int Count { get; set; }

        public string KindName { get; set; }
        public string OwnerName { get; set; }

        public override bool Equals(object obj)
        {
            Offer another = (Offer)obj;
            return another.KindId == this.KindId && another.OwnerId == this.OwnerId 
                && another.PublicationDate.Equals(this.PublicationDate);
        }
    }
}
