﻿
namespace PlantsApp.Entities
{
    public class Kind
    {
        public int KindId { get; set; }
        public string Name { get; set; }
        public string LatinName { get; set; }
        public string Info { get; set; }
        public int StartPrice { get; set; }
        public int FamilyId { get; set; }
        public string FamilyName { get; set; }
    }
}
