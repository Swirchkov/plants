﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantsApp.Entities
{
    public class FilteringModel
    {
        public string Column { get; set; }
        public string Value { get; set; }
    }
}
