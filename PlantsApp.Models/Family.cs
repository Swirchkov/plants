﻿
namespace PlantsApp.Entities
{
    public class Family
    {
        public int FamilyId { get; set; }
        public string Name { get; set; }
        public string LatinName { get; set; }
        public string Info { get; set; }
    }
}
