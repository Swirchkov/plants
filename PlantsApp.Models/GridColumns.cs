﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlantsApp.Entities
{
    public static class GridColumns
    {
        public static string Name => "Name";
        public static string LatinName => "Latin name";
        public static string Info => "Info";
        public static string FamilyName => "Family name";
        public static string StartPrice => "Start price";
        public static string KindName => "Kind name";
        public static string OwnerName => "Owner name";
        public static string PublicationDate => "Publication date";
        public static string Count => "Count";
        public static string Price => "Price";
        public static string Surname => "Surname";
        public static string Phone => "Phone";
        public static string Address => "Address";
    }
}
